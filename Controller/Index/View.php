<?php 
namespace Ktpl\Brand\Controller\Index;

use \Magento\Framework\App\Action\Action as controlleraction;

class View extends controlleraction
{
    private $_resultPageFactory;

    /**
    * Constructor
    *
    * @param \Magento\Framework\App\Action\Context $context
    * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
    */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $result = $this->_resultPageFactory->create();
        return $result;
    }
}
?>