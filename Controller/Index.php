<?php
    /**
    * Copyright © 2015 Magento. All rights reserved.
    * See COPYING.txt for license details.
    */
    namespace Ktpl\Brand\Controller;

    use Magento\Framework\Exception\NotFoundException;
    use Magento\Framework\App\RequestInterface;
    use Magento\Store\Model\ScopeInterface;

    /**
    * Contact index controller
    */
    class Index extends \Magento\Framework\App\Action\Action
    {

        /**
        * @param \Magento\Framework\App\Action\Context $context
        * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
        * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
        * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
        * @param \Magento\Store\Model\StoreManagerInterface $storeManager
        */
        public function __construct(
            \Magento\Framework\App\Action\Context $context
        ) {
            parent::__construct($context);

        }



    }
