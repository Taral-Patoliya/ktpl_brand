<?php
namespace Ktpl\Brand\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magento\Backend\App\Action
{

    /**
    * @param Action\Context $context
    */
    protected $_brandCollectionFactory;
    public function __construct(Action\Context $context,\Ktpl\Brand\Model\Resource\Brand\CollectionFactory $brandCollectionFactory)
    {
        $this->_brandCollectionFactory = $brandCollectionFactory;
        parent::__construct($context); 
    }

    /**
    * Save action
    *
    * @return \Magento\Framework\Controller\ResultInterface
    */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $collection = $this->_brandCollectionFactory->create();
        $collection = $collection->addFieldToFilter('brand_value',array('eq'=>$data['brand_value']));
        $brand_data=$collection->getData();
        if(count($collection)>0 && $this->getRequest()->getParam('brand_id')!=$brand_data[0]['brand_id']){
            $this->messageManager->addError('Brand Already Exists');
            $this->_redirect('*/*/',array('store'=>$data['store_id']));
        }else{
            /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            if ($data) {
                $model = $this->_objectManager->create('Ktpl\Brand\Model\Brand');
                $id = $this->getRequest()->getParam('brand_id');
                if ($id) {
                    $model->load($id);
                }
                /**
                * Save image upload
                */
                try {
                    /* Brand Image*/
                    $uploader = $this->_objectManager->create(
                        'Magento\MediaStorage\Model\File\Uploader',
                        ['fileId' => 'image_path']
                    );
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();

                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(false);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                    ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save($mediaDirectory->getAbsolutePath(\Ktpl\Brand\Model\Brand::BASE_MEDIA_PATH));
                    $data['image_path'] = \Ktpl\Brand\Model\Brand::BASE_MEDIA_PATH . $result['file'];
                    /* END */

                    /*Brand Banner*/
                    $uploader = $this->_objectManager->create(
                        'Magento\MediaStorage\Model\File\Uploader',
                        ['fileId' => 'banner']
                    );
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();

                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(false);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                    ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save($mediaDirectory->getAbsolutePath(\Ktpl\Brand\Model\Brand::BASE_BANNER_PATH));
                    $data['banner'] = \Ktpl\Brand\Model\Brand::BASE_BANNER_PATH . $result['file'];
                    /* END */

                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                    if (isset($data['image_path']) && isset($data['image_path']['value'])) {
                        if (isset($data['image_path']['delete'])) {
                            $data['image_path'] = null;
                            $data['delete_image'] = true;
                        } else if (isset($data['image_path']['value'])) {
                            $data['image_path'] = $data['image_path']['value'];
                        } else {
                            $data['image_path'] = null;
                        }
                    }

                    if (isset($data['banner']) && isset($data['banner']['value'])) {
                        if (isset($data['banner']['delete'])) {
                            $data['banner'] = null;
                            $data['delete_image'] = true;
                        } else if (isset($data['banner']['value'])) {
                            $data['banner'] = $data['banner']['value'];
                        } else {
                            $data['banner'] = null;
                        }
                    }
                }

                /* Get option label from option id*/
                $_product = $this->_objectManager->create('Magento\Catalog\Model\Product');
                $attr = $_product->getResource()->getAttribute("brand");
                if ($attr->usesSource()) {
                    $brand_label = $attr->getSource()->getOptionText($data['brand_value']);
                }

                $data['brand_name'] = $brand_label;
                $data['url_key'] = strtolower(trim(str_replace(' ','-',$brand_label))).".html";

                /* END */
                $model->setData($data);

                try {
                    $model->save();

                    $title = strtolower(trim(str_replace(' ','-',$brand_label)));
                    $requestPath = $title . '.html';
                    $UrlRewritemodel = $this->_objectManager->create('Magento\UrlRewrite\Model\UrlRewrite'); 
                    $urlCollection = $UrlRewritemodel->getCollection()->addFieldToFilter('request_path',$requestPath)->getData();
                    if(count($urlCollection)){
                        $uid = $urlCollection[0]['url_rewrite_id'];
                    }else{
                        $uid = 0;
                    }
                    $rewrite = $this->_objectManager->create('Magento\UrlRewrite\Model\UrlRewrite')->load($uid); 
                    $rewrite->setEntityType('custom')
                    ->setRequestPath($requestPath)
                    ->setTargetPath('brand/index/view/id/' . $model->getId())
                    ->setRedirectType(0)
                    ->setStoreId(1)
                    ->save();

                    $this->messageManager->addSuccess(__('The brand has been saved.'));
                    $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                    if ($this->getRequest()->getParam('back')) {
                        return $resultRedirect->setPath('*/*/edit', ['brand_id' => $model->getId(), '_current' => true]);
                    }
                    return $resultRedirect->setPath('*/*/');
                } catch (\Magento\Framework\Exception\LocalizedException $e) {
                    $this->messageManager->addError($e->getMessage());
                } catch (\RuntimeException $e) {
                    $this->messageManager->addError($e->getMessage());
                } catch (\Exception $e) {
                    $this->messageManager->addException($e, __('Something went wrong while saving the entry.'));
                }

                $this->_getSession()->setFormData($data);
                return $resultRedirect->setPath('*/*/edit', ['brand_id' => $this->getRequest()->getParam('brand_id')]);
            }
            return $resultRedirect->setPath('*/*/');
        }
    }
    protected function _isAllowed()
    {
        return true;
    }
}
