<?php
namespace Ktpl\Brand\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
    * @var PageFactory
    */
    protected $resultPageFactory;

    /**
    * @param Context $context
    * @param PageFactory $resultPageFactory
    */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
    * Index action
    *
    * @return \Magento\Backend\Model\View\Result\Page
    */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        /*$om =  \Magento\Framework\App\ObjectManager::getInstance();
        $manager = $om->get('Magento\Catalog\Model\Product')->getResource()->getAttribute("brand");
        if ($manager->usesSource()) {
            $brand_name = $manager->getSource()->getOptionText(229);
        }
        $isPost['name'] = $brand_name;*/
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Ktpl_Brand::brand');
        $resultPage->addBreadcrumb(__('Ktpl'), __('Brand'));
        $resultPage->addBreadcrumb(__('Manage Brand'), __('Manage Brand'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Brand'));

        return $resultPage;
    }
    protected function _isAllowed()
    {
        return true;
    }
}