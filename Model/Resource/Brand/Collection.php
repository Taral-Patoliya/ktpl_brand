<?php 
namespace Ktpl\Brand\Model\Resource\Brand;

use Magento\Framework\Model\Resource\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Ktpl\Brand\Model\Brand',
            'Ktpl\Brand\Model\Resource\Brand'
        );
    }
}