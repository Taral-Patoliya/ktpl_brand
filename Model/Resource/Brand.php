<?php 
namespace Ktpl\Brand\Model\Resource;

use Magento\Framework\Model\Resource\Db\AbstractDb;

class Brand extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('brands', 'brand_id');
    }
}