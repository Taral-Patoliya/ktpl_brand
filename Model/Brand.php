<?php 
namespace Ktpl\Brand\Model;

use Magento\Framework\Model\AbstractModel;

class Brand extends AbstractModel
{
    const BASE_MEDIA_PATH = 'brand/images/';
    const BASE_BANNER_PATH = 'brand/banner/';
    protected function _construct()
    {
        $this->_init('Ktpl\Brand\Model\Resource\Brand');
    }
}