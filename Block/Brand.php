<?php
namespace Ktpl\Brand\Block;

use Ktpl\Brand\Model\BrandFactory;

class Brand extends \Magento\Framework\View\Element\Template
{
    /**
    * @var \Training3\ExchangeRate\Helper\Data
    */
    protected $_modelBrandFactory;
    protected $_brandCollectionFactory;
    protected $_productFactory;
    protected $_categoryFactory;
    /*
    * Define template file
    */
    /**
    * Constructor
    * 
    * @param \Magento\Framework\View\Element\Template\Context $context
    * @param \Training3\ExchangeRate\Helper\Data $rateHelper
    * @param array $data
    */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Ktpl\Brand\Model\Resource\Brand\CollectionFactory $brandCollectionFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        BrandFactory $modelBrandFactory,
        array $data = array()
    ) {
        $this->_brandCollectionFactory = $brandCollectionFactory;
        $this->_productFactory = $productFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->_modelBrandFactory = $modelBrandFactory;
        parent::__construct($context, $data);
    }

    public function getBrands(){
        $collection = $this->_brandCollectionFactory->create();
        $collection = $collection->addFieldToFilter('is_active',array('eq'=>1));
        return $collection;
    }

    public function getBrandUrl($_brand){
        return $this->getUrl($_brand->getUrlKey());
    }

    public function getBrandImage($_brand){
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $_brand->getImagePath();
    }

    public function getBrandBanner($_brand){
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $_brand->getBanner();
    }

    public function getBrandInfo(){
        $brand_id = $this->getRequest()->getParam('id');
        $brandModel = $this->_modelBrandFactory->create();
        $brand = $brandModel->load($brand_id);
        return $brand;

    }

    public function getProductCollection($_brand){
        $product = $this->_productFactory
        ->create()
        ->getCollection()
        ->addFieldToFilter('brand',$_brand->getBrandValue());

        return $product;

    }

    public function getCategoryCollection($category_ids){
        //$category_ids = implode(",",$category_ids);
        $category = $this->_categoryFactory
        ->create()
        ->getCollection()
        ->addAttributeToSelect('*')
        ->addFieldToFilter('entity_id',array('in'=>array($category_ids)))
        ->addFieldToFilter('level',array('neq'=>2))
        ->addFieldToFilter('is_active',1);

        return $category;
    }
}

