<?php

namespace Ktpl\Brand\Block\Adminhtml\Form\Edit\Tab;

/**
* Blog post edit form main tab
*/
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
    * @var \Magento\Store\Model\System\Store
    */
    protected $_systemStore;

    /**
    * @var \Magento\Cms\Model\Wysiwyg\Config
    */
    protected $_wysiwygConfig;

    protected $_formFactory;


    protected $_status;

    /**
    * @param \Magento\Backend\Block\Template\Context $context
    * @param \Magento\Framework\Registry $registry
    * @param \Magento\Framework\Data\FormFactory $formFactory
    * @param \Magento\Store\Model\System\Store $systemStore
    * @param array $data
    */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Ktpl\Brand\Model\Status $status,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_status = $status;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
    * Prepare form
    *
    * @return $this
    * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
    */
    protected function _prepareForm()
    {

        $model = $this->_coreRegistry->registry('form_post');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Brand Information')]);

        if ($model->getId()) {
            $fieldset->addField('brand_id', 'hidden', ['name' => 'brand_id']);
        }

        $object_manager = \Magento\Framework\App\ObjectManager::getInstance();
        $attribute = $object_manager->get('Magento\Eav\Model\Config')->getAttribute('catalog_product','brand');
        $options = $attribute->getSource()->getAllOptions(false);
        $default=array('value'=>'','label'=>'Choose Brand');
        $i=0;
        $brand[$i]=$default;
        foreach($options as $key=>$value){
            $i++;
            $brand[$i]=$value;
        }

        $fieldset->addField(
            'brand_value',
            'select',
            [
                'name'        => 'brand_value',
                'label'    => __('Brand'),
                'required'     => true,
                'values'    =>$brand, 
            ]
        );

        $wysiwygConfig = $this->_wysiwygConfig->getConfig();
        $fieldset->addField(
            'description',
            'editor',
            [
                'name' => 'description',
                'label' => __('Description'),
                'title' => __('Description'),
                'required' => true,
                'disabled' => $isElementDisabled,
                'value' =>'abc',
                'config'    => $wysiwygConfig
            ]
        );

        $fieldset->addField(
            'image_path',
            'image',
            [
                'name' => 'image_path',
                'label' => __('Image Path'),
                'title' => __('Image Path'),
                'required' => false,
                'disabled' => $isElementDisabled,
                'value' =>'abc'
            ]
        );
        
        $fieldset->addField(
            'banner',
            'image',
            [
                'name' => 'banner',
                'label' => __('Banner'),
                'title' => __('Banner'),
                'required' => false,
                'disabled' => $isElementDisabled,
                'value' =>'abc'
            ]
        );


        $fieldset->addField(
            'is_active',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'is_active',
                'required' => true,
                'options' => $this->_status->getOptionArray(),
                'disabled' => $isElementDisabled
            ]
        );
        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
    * Prepare label for tab
    *
    * @return \Magento\Framework\Phrase
    */
    public function getTabLabel()
    {
        return __('Brand');
    }

    /**
    * Prepare title for tab
    *
    * @return \Magento\Framework\Phrase
    */
    public function getTabTitle()
    {
        return __('Brand');
    }

    /**
    * {@inheritdoc}
    */
    public function canShowTab()
    {
        return true;
    }

    /**
    * {@inheritdoc}
    */
    public function isHidden()
    {
        return false;
    }

    /**
    * Check permission for passed action
    *
    * @param string $resourceId
    * @return bool
    */
    /*protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }*/
}
