<?php

namespace Ktpl\Brand\Block\Adminhtml\Form\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config;

class Info extends Generic implements TabInterface
{
    /**
    * @var \Magento\Cms\Model\Wysiwyg\Config
    */
    protected $_wysiwygConfig;
    protected $objectManager;
    protected $_brandStatus;
    /**
    * @param Context $context
    * @param Registry $registry
    * @param FormFactory $formFactory
    * @param Config $wysiwygConfig
    * @param array $data
    */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Config $wysiwygConfig,
        \Ktpl\Brand\Model\Status $status,
        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_brandStatus = $status;
        parent::__construct($context, $registry, $formFactory, $data);
        $this->objectManager = $objectManager;
    }

    /**
    * Prepare form fields
    *
    * @return \Magento\Backend\Block\Widget\Form
    */
    protected function _prepareForm()
    {
        $form               =   $this->_formFactory->create();
        $form->setHtmlIdPrefix('brand_');
        $form->setFieldNameSuffix('brand');
        $model              =   $this->_coreRegistry->registry('brand_brand');
        $om                 =   \Magento\Framework\App\ObjectManager::getInstance();
        $reader             =   $om->get('Magento\Eav\Model\Config')->getAttribute('catalog_product','brand');
        $attributeOptions   =   $reader->getSource()->getAllOptions(true, true);
        $default            =   array('value'=>'','label'=>'Choose Brand');
        $i=0;
        $brand[$i]   =   $default;
        foreach($attributeOptions as $key=>$value){
            $i++;  
            if($key!=0){
                $brand[$i]=$value; 
                //$name[$i] = $value['label'];
            }

        }
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General')]
        );

        if ($model->getId()) {
            $fieldset->addField(
                'id',
                'hidden',
                ['name' => 'id']
            );
        }
        $fieldset->addField(
            'mfc_id',
            'select',
            [
                'name'        => 'mfc_id',
                'label'    => __('Brand'),
                'required'     => true,
                'values'    =>$brand, 
            ]
        );


        $fieldset->addField(
            'status',
            'select',
            [
                'name'      => 'status',
                'label'     => __('Status'),
                'options'   => $this->_brandStatus->getOptionArray()
            ]
        );
       $fieldset->addField(
            'position',
            'text',
            [
                'name'        => 'position',
                'label'    => __('Position'),
                'required'     => true,
                'style' => 'width:600px;',    

            ]
        );

        $fieldset->addField(
            'image_path',
            'image',
            array(
                'name' => 'image_path',
                'label' => __('Image'),
                'title' => __('Image')
            )
        );
        $fieldset->addField(
            'mfc_banner',
            'image',
            array(
                'name' => 'mfc_banner',
                'label' => __('Banner'),
                'title' => __('Banner')
            )
        );
        $wysiwygConfig = $this->_wysiwygConfig->getConfig();
        $fieldset->addField(
            'description',
            'editor',
            [
                'name'        => 'description',
                'label'    => __('Description'),
                'required'     => true,
                'config'    => $wysiwygConfig
            ]
        );

        $wysiwygConfig = $this->_wysiwygConfig->getConfig();
        $fieldset->addField(
            'url_key',
            'text',
            [
                'name'        => 'url_key',
                'label'    => __('Url Key'),
                'required'     => true,
                'style' => 'width:600px;',    

            ]
        );


        $data = $model->getData();
        $form->setValues($data);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
    * Prepare label for tab
    *
    * @return string
    */
    public function getTabLabel()
    {
        return __('Brand Info');
    }

    /**
    * Prepare title for tab
    *
    * @return string
    */
    public function getTabTitle()
    {
        return __('Brand Info');
    }

    /**
    * {@inheritdoc}
    */
    public function canShowTab()
    {
        return true;
    }

    /**
    * {@inheritdoc}
    */
    public function isHidden()
    {
        return false;
    }
}
