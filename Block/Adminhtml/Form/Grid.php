<?php
namespace Ktpl\Brand\Block\Adminhtml\Form;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
    * @var \Magento\Framework\Module\Manager
    */
    protected $moduleManager;

    /**
    * @var \Ktpl\Customform\Model\FormFactory
    */
    protected $_brandFactory;

    protected $_status;

    /**
    * @param \Magento\Backend\Block\Template\Context $context
    * @param \Magento\Backend\Helper\Data $backendHelper
    * @param \Ktpl\Customform\Model\FormFactory $formFactory
    * @param \Ktpl\Customform\Model\Status $status
    * @param \Magento\Framework\Module\Manager $moduleManager
    * @param array $data
    *
    * @SuppressWarnings(PHPMD.ExcessiveParameterList)
    */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Ktpl\Brand\Model\BrandFactory $brandFactory,
        \Ktpl\Brand\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_brandFactory = $brandFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
    * @return void
    */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('BrandGrid');
        $this->setDefaultSort('brand_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('post_filter');
    }

    /**
    * @return $this
    */
    protected function _prepareCollection()
    {
        $collection = $this->_brandFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
    * @return $this
    * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
    */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'brand_id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'brand_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'name'=>'news_id'
            ]
        );
        $this->addColumn(
            'brand_name',
            [
                'header' => __('Brand Name'),
                'index' => 'brand_name',
                'class' => 'xxx',
                'name'=>'title'
            ]
        );

        $this->addColumn(
            'image_path',
            [
                'header' => __('Image'),
                'class' => 'xxx',
                'width' => '100px',
                'filter' => false,
                'renderer' => 'Ktpl\Brand\Block\Adminhtml\Form\Helper\Renderer\Image',
            ]
        );

        $this->addColumn(
            'is_active',
            [
                'header' => __('Active'),
                'index' => 'is_active',
                'type' => 'options',
                'name'=>'is_active',
                'options' => $this->_status->getOptionArray()
            ]
        );


        $this->addColumn(
            'edit',
            [
                'header' => __('Edit'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Edit'),
                        'url' => [
                            'base' => '*/*/edit'
                        ],
                        'field' => 'brand_id'
                    ]
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action'
            ]
        );

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
    * @return $this
    */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('brand_id');
        $this->getMassactionBlock()->setTemplate('Ktpl_Brand::form/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('brand_id');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('brand/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        //array_unshift($statuses, ['label' => '', 'value' => '']);

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('brand/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }

    /**
    * @return string
    */
    public function getGridUrl()
    {
        return $this->getUrl('brand/*/grid', ['_current' => true]);
    }

    public function getRowUrl($row)
    {
        return $this->getUrl(
            'brand/*/edit',
            ['brand_id' => $row->getId()]
        );
    }
}