<?php
    /**
    * @Author: zerokool - Nguyen Huu Tien
    * @Email: tien.uet.qh2011@gmail.com
    * @File Name: Image.php
    * @File Path: /home/zero/public_html/magento2/1.0.0-beta_v1/app/code/Magestore/Bannerslider/Block/Adminhtml/Banner/Helper/Renderer/Image.php
    * @Date:   2015-04-09 11:17:51
    * @Last Modified by:   zero
    * @Last Modified time: 2015-07-24 11:39:09
    */
    namespace Ktpl\Brand\Block\Adminhtml\Form\Helper\Renderer;
    class Image extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {
        /**
        * Store manager
        *
        * @var \Magento\Store\Model\StoreManagerInterface
        */
        protected $_storeManager;

        protected $_brandFactory;

        /**
        * Registry object
        * @var \Magento\Framework\Registry
        */
        protected $_coreRegistry;

        /**
        * @param \Magento\Backend\Block\Context $context
        * @param array $data
        */
        public function __construct(
            \Magento\Backend\Block\Context $context,
            \Magento\Store\Model\StoreManagerInterface $storeManager,
            \Ktpl\Brand\Model\BrandFactory $brandFactory,
            \Magento\Framework\Registry $coreRegistry,
            array $data = []
        ) {
            parent::__construct($context, $data);
            $this->_storeManager = $storeManager;
            $this->_brandFactory = $brandFactory;
            $this->_coreRegistry = $coreRegistry;
        }

        /**
        * Render action
        *
        * @param \Magento\Framework\Object $row
        * @return string
        */
        public function render(\Magento\Framework\Object $row) {
            $storeViewId = $this->getRequest()->getParam('store');
            $brand = $this->_brandFactory->create()->load($row->getId());
            $srcImage = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $brand->getImagePath();
            return '<image width="150" height="100" src ="' . $srcImage . '" alt="' . $brand->getImagePath() . '" >';
        }
    }
