<?php namespace Ktpl\Brand\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
    * Installs DB schema for a module
    *
    * @param SchemaSetupInterface $setup
    * @param ModuleContextInterface $context
    * @return void
    */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
        ->newTable($installer->getTable('brands'))
        ->addColumn(
            'brand_id',
            Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Brand ID'
        )
        ->addColumn('brand_name', Table::TYPE_TEXT, 255, ['nullable' => false], 'Brand Name')
        ->addColumn('brand_value', Table::TYPE_SMALLINT, null, [], 'Brand Value')
        ->addColumn('is_active', Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '1'], 'Is Active?')
        ->addColumn('image_path', Table::TYPE_TEXT, 255, ['nullable' => true], 'Image Path')
        ->addColumn('banner', Table::TYPE_TEXT, 255, ['nullable' => true], 'Banner Image')
        ->addColumn('description', Table::TYPE_TEXT, 255, ['nullable' => true], 'Description')
        ->addColumn('url_key', Table::TYPE_TEXT, 255, ['nullable' => true], 'URL Key')
        ->addColumn('creation_time', Table::TYPE_DATETIME, null, ['nullable' => false], 'Creation Time')
        ->addColumn('update_time', Table::TYPE_DATETIME, null, ['nullable' => false], 'Update Time')
        ->setComment('Brand Module');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}